# Semapps

SemApps is an open source projet which aim to **ease data storage and filtering**.
SemApps means Semantinc App thus storage and filtering are ontology driven

More information about the team : http://www.virtual-assembly.org/

We welcome contributors from everywhere. Please contact us to join the team 
- we use Riot/Matrix to communicate
- we use Loomio/framavox to take decision https://framavox.org/join/group/dy8uNV8qvFBTisLVB5yPkmtc/



## Documentation (qu'on mettra sur un SemApps dédié un jour !)
### Interne
* [Comparatif des technos disponibles](https://hackmd.lescommuns.org/s/SJZxc-Yqr)
* [Hackmd principal](https://hackmd.lescommuns.org/EwZgrAnBCMBsAMBaAxgMxMxAWMyCGiARgOwAmmYpxhexYsshAHIUA===?both#)


### Externe

* [Thèse de recherche](https://csarven.ca/linked-research-decentralised-web) 